const CategoryController = require("../controllers/category.controller");
const verifyToken = require("../utils/verifyToken");
const express = require("express");
const router = express.Router();

router.get("/", CategoryController.getCategories);
router.post("/add", verifyToken, CategoryController.addCategory);
router.put("/update/:id", verifyToken, CategoryController.updateCategory);
router.delete("/:id", verifyToken, CategoryController.deleteCategory);
router.put("/:id/sub/add", CategoryController.addSub);
router.put("/:id/sub/delete/:subid", CategoryController.deleteSub);
router.put("/:id/sub/update/:subid", CategoryController.updateSub);
module.exports = router;
