const dotenv = require("dotenv");
dotenv.config();
const router = require("express").Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../models/user.models");
const Cart = require("../models/cart.models");
const passwordResetToken = require("../models/resetpassword");
const nodemailer = require("nodemailer");
const crypto = require("crypto");
router.post("/register", async (req, res) => {
  //Validating the data we parse in body
  //Checking if the email is validation
  try {
    const existEmail = await User.findOne({ email: req.body.email });
    if (existEmail) {
      res.json({ err: "Email already exist" });
    } else {
      const salt = await bcrypt.genSalt(16);
      const hashedPassword = await bcrypt.hash(req.body.password, salt);
      //Creating new user
      const user = new User({
        fullname: req.body.fullname,
        email: req.body.email,
        phoneNumber: req.body.phonenumber,
        address: req.body.address,
        isAdmin: req.body.isAdmin,
        password: hashedPassword,
      });

      await user.save();
      const newCart = new Cart({
        owner: user._id,
      });

      await newCart.save();
      let result = await User.findByIdAndUpdate(
        user._id,
        { cart: newCart._id },
        { new: true }
      );

      res.json({ user: result.authToJSON() });
    }
  } catch (err) {
    console.log(err);
    res.status(400).json({ err });
  }
  //Hashing the password
});
router.post("/login", async (req, res) => {
  //Validating the data
  //Checking if email is valid
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.send({ err: "Wrong Email or Password" });
  //Validate password
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.send({ err: "Wrong Email or Password" });
  //Generating Token
  const token = jwt.sign(
    { _id: user._id, isFarmer: user.isFarmer, role: user.isAdmin },
    process.env.TOKEN_KEY_PASS,
    { expiresIn: "2 days" }
  );
  res
    .header("access_token", token)
    .json({ message: "login valid", token: token, user: user.authToJSON() });
});
router.post("/resetpassword", async (req, res) => {
  if (!req.body.email) {
    return res.status(500).json({ message: "Email is required" });
  }
  const user = await User.findOne({
    email: req.body.email,
  });
  if (!user) {
    return res.status(409).json({ message: "Email does not exist" });
  }
  var resettoken = new passwordResetToken({
    _userId: user._id,
    resettoken: crypto.randomBytes(16).toString("hex"),
  });
  resettoken.save(function (err) {
    if (err) {
      return res.status(500).send({ msg: err.message });
    }
    passwordResetToken
      .find({ _userId: user._id, resettoken: { $ne: resettoken.resettoken } })
      .remove()
      .exec();
    res.status(200).json({ message: "Reset Password successfully." });
    var transporter = nodemailer.createTransport({
      service: "in-v3.mailjet.com",
      port: 587,
      auth: {
        user: "0aa9cdbe3208912d139cd3f85e7b1597",
        pass: "5268b7eebabd7486b21d88a25b10a829",
      },
    });
    var mailOptions = {
      to: user.email,
      from: "in-v3.mailjet.com",
      subject: "Node.js Password Reset",
      text:
        "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
        "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
        "http://localhost:4200/response-reset-password/" +
        resettoken.resettoken +
        "\n\n" +
        "If you did not request this, please ignore this email and your password will remain unchanged.\n",
    };
    transporter.sendMail(mailOptions, (err, info) => {
      console.log(err);
    });
  });
});

router.post("/validatepassword", async (req, res) => {
  if (!req.body.resettoken) {
    return res.status(500).json({ message: "Token is required" });
  }
  const user = await passwordResetToken.findOne({
    resettoken: req.body.resettoken,
  });
  if (!user) {
    return res.status(409).json({ message: "Invalid URL" });
  }
  User.findOneAndUpdate({ _id: user._userId })
    .then(() => {
      res.status(200).json({ message: "Token verified successfully." });
    })
    .catch((err) => {
      return res.status(500).send({ msg: err.message });
    });
});

router.post("/newpass", async (req, res) => {
  passwordResetToken.findOne({ resettoken: req.body.resettoken }, function (
    err,
    userToken,
    next
  ) {
    if (!userToken) {
      return res.status(409).json({ message: "Token has expired" });
    }

    User.findOne(
      {
        _id: userToken._userId,
      },
      function (err, userEmail, next) {
        if (!userEmail) {
          return res.status(409).json({ message: "User does not exist" });
        }
        return bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
          if (err) {
            return res.status(400).json({ message: "Error hashing password" });
          }
          userEmail.password = hash;
          userEmail.save(function (err) {
            if (err) {
              return res
                .status(400)
                .json({ message: "Password can not reset." });
            } else {
              userToken.remove();
              return res
                .status(201)
                .json({ message: "Password reset successfully" });
            }
          });
        });
      }
    );
  });
});
module.exports = router;
