const messageController = require("../controllers/message.controllers");
const router = require("express").Router();

router.post("/add/message", messageController.addMessage);
router.get("/", messageController.getAllMessages);

module.exports = router;
