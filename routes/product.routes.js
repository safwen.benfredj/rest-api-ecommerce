const router = require("express").Router();
const productControllers = require("../controllers/product.controllers");
const Product = require("../models/product.models");
const verifyToken = require("../utils/verifyToken");
const isFarmer = require("../utils/isFarmer");
const multer = require("multer");
// const upload = multer( { dest: "uploads/" } );

//Image upload setting
const PATH = "uploads/";
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, PATH);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      new Date().toISOString().replace(/:/g, "-") + "-" + file.originalname
    );
  },
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
});
// preload story object on routes with ':story'
router.param("product", async (req, res, next, id) => {
  try {
    const product = await Product.findOne({ _id: id }).populate({
      path: "farmer",
      select: "_id",
    });

    if (!product) return res.sendStatus(404);
    req.product = product;
    return next();
  } catch (err) {
    res.json({ err_message: err });
  }
});

// Product routing
router.get("/:Category", productControllers.getProducts);
router.get("/", productControllers.getAllProducts);
router.get("/:product", productControllers.getProduct);

router.post("/searchproduct", async (req, res) => {
  try {
    const toSearch = req.body.productName;
    toSearch.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    const product = await Product.find({
      productName: new RegExp(toSearch, "gi"),
    });
    if (!product) return res.json({ err: "Not found" });
    res.json(product);
  } catch (ex) {
    console.log(ex);
  }
});

router.post(
  "/create",
  upload.single("productImage"),
  verifyToken,
  productControllers.createProduct
);

router.put(
  "/:product/update",
  upload.single("productImage"),
  verifyToken,
  productControllers.updateProduct
);

router.delete(
  "/:product/delete",
  upload.single("productImage"),
  verifyToken,
  productControllers.deleteProduct
);
router.get(
  "/find/:categrory",
  verifyToken,
  productControllers.getProductsByCateg
);
router.get("/prods/find/:subcateg", productControllers.findProductsBySub);
router.get("/latest/items", productControllers.getLatestProducts);
router.put("/promotion/set/:id", productControllers.setPromotion);
router.get("/promotions/all", productControllers.promotionProducts);
module.exports = router;
