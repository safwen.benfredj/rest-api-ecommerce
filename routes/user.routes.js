const dotenv = require("dotenv");
dotenv.config();
const router = require("express").Router();
const verifyToken = require("../utils/verifyToken");
const userController = require("../controllers/user.controllers");
const UserModal = require("../models/user.models");
const OrderModal = require("../models/order.models");
router.get("/", verifyToken, userController.getUsers);
router.put("/:id", verifyToken, userController.updateUser);
router.delete("/:id", verifyToken, userController.deleteUser);
router.get("/:id", verifyToken, userController.getUser);
router.get("/users/admin", verifyToken, userController.getAdmins);
router.get("/clients/test", async (req, res) => {
  let resultat = [];
  let users = await UserModal.find({
    isAdmin: false,
  });
  const promise = new Promise((resolve, reject) => {
    users.forEach(async (elm) => {
      let totalOrders = await OrderModal.find({ owner: elm._id });
      let Client = {
        user: elm,
        totalCount: totalOrders.length,
        lastOrder: totalOrders[totalOrders.length],
      };
      resultat.push(Client);
      //console.log(users);
    });
    setTimeout(() => {
      resolve(resultat);
    }, 1000);
  });

  promise.then((value) => {
    console.log("Final Result", value);
    res.json(value);
  });

  //res.json(resultat);
});

router.get("/searchuser", async (req, res) => {
  try {
    const toSearch = req.params.user;
    toSearch.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    const product = await UserModal.find({
      productName: new RegExp(toSearch, "gi"),
    });
    if (!product) return res.json({ err: "Not found" });
    res.json(product);
  } catch (ex) {
    console.log(ex);
  }
});
module.exports = router;
