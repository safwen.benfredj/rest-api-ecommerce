const router = require("express").Router();
const Delivery = require("../models/delivery.model");
router.post("/add", async (req, res) => {
  let delivery = new Delivery({
    price: req.body.price,
  });
  let result = await delivery.save();
  res.json(result);
});
router.get("/", async (req, res) => {
  let result = await Delivery.findOne();
  res.json(result);
});

router.put("/update/:id", async (req, res) => {
  console.log("My Id", req.params.id);
  let result = await Delivery.findByIdAndUpdate(req.params.id, {
    price: req.body.newPrice,
  });
  res.json(result);
});
module.exports = router;
