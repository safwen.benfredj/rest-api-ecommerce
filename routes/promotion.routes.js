const promotionController = require("../controllers/promotion.controller");
const router = require("express").Router();
const Product = require("../models/product.models");
const verifyToken = require("../utils/verifyToken");
const isFarmer = require("../utils/isFarmer");
const multer = require("multer");
const PATH = "uploads/";
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, PATH);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      new Date().toISOString().replace(/:/g, "-") + "-" + file.originalname
    );
  },
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
  fileFilter: fileFilter,
});
router.get("/", promotionController.getPromotions);
router.post(
  "/add",
  verifyToken,
  upload.single("promotionImage"),
  promotionController.createPromotion
);
router.put(
  "/update/:id",
  verifyToken,
  upload.single("promotionImage"),
  promotionController.updatePromotion
);
router.delete("/:id", verifyToken, promotionController.deletePromotion);
module.exports = router;
