const path = require("path");
const express = require("express");
const app = express();
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const cors = require("cors");

//cors Config
// const corsOptions = {
// 	origin: "http://localhost:4200",
// 	optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
// 	credentials: true
// };
//Dotenv Config
dotenv.config();

//Connect to database
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose
  .connect(process.env.MONGO_DATABASE_URI, {
    autoReconnect: false,
    keepAlive: false,
    connectTimeoutMS: 3000000,
    socketTimeoutMS: 3000000,
  })
  .catch((err) => {
    console.log(err);
  });
mongoose.connection.on("connected", () => console.log("Connected"));
mongoose.connection.on("error", (err) =>
  console.log("Connection failed with - ", err)
);
// () => console.log( "connected to db" ) ).catch( err => {

// 	console.log( "Mongo err", err );
// } );

//Importe routes
const productRoute = require("./routes/product.routes");
const authRoute = require("./routes/auth.routes");
const orderRoute = require("./routes/order.routes");
const cartRoute = require("./routes/cart.routes");
const categrieRoute = require("./routes/category.routes");
const userRoute = require("./routes/user.routes");
const promotionRoute = require("./routes/promotion.routes");
const messageRoute = require("./routes/message.routes");
const deliveryRoute = require("./routes/delivery.routes");
//Middleware
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(cors());

app.use("/uploads", express.static(path.join(__dirname + "/uploads")));
//Route Middleware
app.use("/product", productRoute);
app.use("/auth", authRoute);
app.use("/orders", orderRoute);
app.use("/cart", cartRoute);
app.use("/categorie", categrieRoute);
app.use("/users", userRoute);
app.use("/promotions", promotionRoute);
app.use("/messages", messageRoute);
app.use("/livraison", deliveryRoute);
// Serve static assets if in production

//Server Run
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log("Server is up and running on port number " + port);
});
