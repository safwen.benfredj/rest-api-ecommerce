const Product = require("../models/product.models");

const createProduct = async (req, res) => {
  console.log("Sub Categ", req.body.subCateg);
  const product = new Product({
    productName: req.body.productName,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
    Category: req.body.Category,
    SubCateg: req.body.subCateg,
    productImage: req.file.path,
  });

  try {
    const savedProduct = await product.save();
    return res.json(savedProduct);
  } catch (err) {
    return res.json({ err_message: err });
  }
};

const setPromotion = async (req, res) => {
  console.log(req.params.id);
  let result = await Product.findByIdAndUpdate(req.params.id, {
    InPromotion: req.body.promotion,
    Promotionprice: req.body.newPrice,
  });
  res.json(result);
};

//Get all stories
const getProducts = async (req, res) => {
  try {
    const products = await Product.find({
      Category: req.params.Category,
    }).populate("Category");
    return res.json({ products: products });
  } catch (err) {
    throw res.json({ err_message: err });
  }
};
const getAllProducts = async (req, res) => {
  try {
    const products = await Product.find({}).populate("Category");
    return res.json({ products: products });
  } catch (err) {
    throw res.json({ err_message: err });
  }
};
const getProductsInPromotions = async (req, res) => {
  try {
    const products = await Product.find({ InPromotion: true }).populate(
      "Category"
    );
    return res.json({ products: products });
  } catch (err) {
    throw res.json({ err_message: err });
  }
};
const getLatestPorducts = async (req, res) => {
  try {
    const products = await Product.find().sort({ createdAt: -1 }).limit(4);
    return res.json({ products: products });
  } catch (err) {
    res.json({ err_message: err });
  }
};
//Get single story
const getProduct = async (req, res) => {
  const product = req.product;

  try {
    return res.json({ product: product });
  } catch (err) {
    res.json({ err_message: err });
  }
};
//update story
const updateProduct = async (req, res) => {
  const product = req.params.product;
  console.log(product);
  let updateData = {};
  try {
    if (req.file !== undefined) {
      updateData = {
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        quantity: req.body.quantity,
        Category: req.body.Category,
        SubCateg: req.body.subCateg,
        productImage: req.file.path,
      };
    } else {
      updateData = {
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        quantity: req.body.quantity,
        Category: req.body.Category,
        SubCateg: req.body.subCateg,
      };
    }
    const updateProduct = await Product.findByIdAndUpdate(product, updateData, {
      new: true,
    });
    console.log(updateProduct);
    return res.json(updateProduct);
  } catch (err) {
    res.json(err);
  }
};
//Get delete story
const deleteProduct = async (req, res) => {
  try {
    const product = req.product;
    const deleteProduct = await Product.findByIdAndDelete(product._id);
    return res.json(deleteProduct);
  } catch (err) {
    res.json({ err_message: err });
  }
};

const findProductByCateg = async (req, res) => {
  try {
    const product = req.params.categrory;
    const result = await Product.find({ Category: product });
    return res.json(result);
  } catch (err) {
    res.json({ err_message: err });
  }
};
const findProductsBySub = async (req, res) => {
  try {
    const product = req.params.subcateg;
    console.log(product);
    const result = await Product.find({ SubCateg: product });
    return res.json(result);
  } catch (err) {
    res.json({ err_message: err });
  }
};
module.exports.createProduct = createProduct;
module.exports.getProduct = getProduct;
module.exports.getProducts = getProducts;
module.exports.updateProduct = updateProduct;
module.exports.deleteProduct = deleteProduct;
module.exports.getAllProducts = getAllProducts;
module.exports.getProductsByCateg = findProductByCateg;
module.exports.getLatestProducts = getLatestPorducts;
module.exports.setPromotion = setPromotion;
module.exports.promotionProducts = getProductsInPromotions;
module.exports.findProductsBySub = findProductsBySub;
