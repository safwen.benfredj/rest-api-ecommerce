const Order = require("../models/order.models");
const Cart = require("../models/cart.models");
const createOrder = async (req, res) => {
  const order = new Order({
    owner: req.body.owner,
    items: req.body.items,
    address: req.body.address,
    total: req.body.total,
  });
  try {
    const savedOrder = await order.save();
    const cartEmpty = await Cart.findOne({ owner: savedOrder.owner });
    let result = await cartEmpty.emptyCart();
    console.log(result);
    return res.json(savedOrder);
  } catch (err) {
    console.log(err);
    return res.json(err);
  }
};

const getOrder = async (req, res) => {
  const order = req.order;

  try {
    return res.json(order);
  } catch (err) {
    res.json({ err_message: err });
  }
};

const getOrders = async (req, res) => {
  const owner = req.owner;
  try {
    const orders = await Order.find({ owner: owner._id }).populate({
      path: "owner",
      select: ["fullname", "phoneNumber"],
    });
    return res.json(orders);
  } catch (err) {
    throw res.json({ err_message: err });
  }
};

const getAllOrders = async (req, res) => {
  try {
    const orders = await Order.find()
      .populate({ path: "owner", select: ["fullname", "phoneNumber"] })
      .populate("items.item");
    return res.json(orders);
  } catch (err) {
    throw res.json({ err_message: err });
  }
};

const getOrderById = async (req, res) => {
  const order = req.order;

  try {
    const orders = await Order.findById(order._id)
      .populate({ path: "owner", select: ["fullname", "phoneNumber"] })
      .populate({
        path: "items.item",
        select: ["productName", "description", "productImage", "quantity"],
      });
    return res.json(orders);
  } catch (err) {
    throw res.json({ err_message: err });
  }
};

const getLastOrderByClient = async (req, res) => {
  try {
    const order = await Order.findOne({
      owner: req.params.id,
    })
      .populate({ path: "owner", select: ["fullname", "phoneNumber"] })
      .sort({ field: -_id });
    return order;
  } catch (er) {}
};

module.exports.filterByState = async (req, res) => {
  try {
    const result = await Order.find({
      isOrderCompleted: req.params.id,
    });
    res.json(result);
  } catch (ex) {}
};
module.exports.getOrderById = getOrderById;
module.exports.getAllOrders = getAllOrders;
module.exports.createOrder = createOrder;
module.exports.getOrder = getOrder;
module.exports.getOrders = getOrders;
module.exports.getOrderByClient = getLastOrderByClient;
