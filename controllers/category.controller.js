const Category = require("../models/category.model");
const mongoose = require("mongoose");
module.exports.addCategory = async (req, res) => {
  let newCategory = Category({
    categoryName: req.body.name,
    subCategories: [],
  });
  let result = await newCategory.save();
  res.json({ result: result });
};

module.exports.getCategories = async (req, res) => {
  let result = await Category.find();

  res.json({ categories: result });
};

module.exports.updateCategory = async (req, res) => {
  let result = await Category.findByIdAndUpdate(
    req.params.id,
    {
      categoryName: req.body.name,
    },
    { new: true }
  );
  res.json({
    result: result,
  });
};

module.exports.deleteCategory = async (req, res) => {
  let result = await Category.findByIdAndRemove(
    req.params.id,

    { new: true }
  );
  res.json({
    result: result,
  });
};

module.exports.addSub = async (req, res) => {
  try {
    let cat = await Category.findById(req.params.id);
    let subs = cat.subCategories;
    let newsuCat = {
      _id: new mongoose.Types.ObjectId(),
      subName: req.body.name,
    };
    let newSubs = [...subs, newsuCat];
    let result = await Category.findByIdAndUpdate(req.params.id, {
      subCategories: newSubs,
    });
    res.json(result);
  } catch (ex) {
    res.json(ex);
  }
};
module.exports.deleteSub = async (req, res) => {
  try {
    let cat = await Category.findById(req.params.id);
    let subs = cat.subCategories;
    let newSubs = subs.filter((elm) => elm._id != req.params.subid);
    let result = await Category.findByIdAndUpdate(req.params.id, {
      subCategories: newSubs,
    });
    res.json(result);
  } catch (ex) {
    res.json(ex);
  }
};

module.exports.updateSub = async (req, res) => {
  try {
    let cat = await Category.findById(req.params.id);
    let subs = cat.subCategories;
    let selectedItem = subs.findIndex((elm) => elm._id == req.params.subid);
    subs[selectedItem].subName = req.body.name;
    console.log(subs);
    let newSubs = subs;
    let result = await Category.findByIdAndUpdate(req.params.id, {
      subCategories: newSubs,
    });
    res.json(result);
  } catch (ex) {
    res.json(ex);
  }
};
