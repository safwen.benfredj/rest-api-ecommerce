const Message = require("../models/message.model");

module.exports.addMessage = async (req, res) => {
  try {
    const newMessage = new Message({
      fullname: req.body.name,
      email: req.body.email,
      message: req.body.message,
    });
    const result = await newMessage.save();
    res.json(result);
  } catch (ex) {}
};

module.exports.getAllMessages = async (req, res) => {
  try {
    const result = await Message.find();
    res.json(result);
  } catch (ex) {}
};
