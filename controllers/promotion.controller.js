const Promotion = require("../models/promotion.model");

module.exports.createPromotion = async (req, res) => {
  const promotion = new Promotion({
    PoromotionTitle: req.body.PoromotionTitle,
    Description: req.body.Description,
    promotionImage: req.file.path,
  });

  try {
    const Savedpromotion = await promotion.save();
    return res.json(Savedpromotion);
  } catch (err) {
    return res.json({ err_message: err });
  }
};
module.exports.getPromotions = async (req, res) => {
  try {
    const promotions = await Promotion.find();
    return res.json(promotions);
  } catch (err) {
    throw res.json({ err_message: err });
  }
};
module.exports.updatePromotion = async (req, res) => {
  const promotion = req.params.id;
  let updateData = {};
  try {
    if (req.file !== undefined) {
      updateData = {
        PoromotionTitle: req.body.PoromotionTitle,
        Description: req.body.Description,
        promotionImage: req.file.path,
      };
    } else {
      updateData = {
        PoromotionTitle: req.body.PoromotionTitle,
        Description: req.body.Description,
      };
    }
    const updatePromotion = await Promotion.findByIdAndUpdate(
      promotion,
      updateData,
      { new: true }
    );
    return res.json(updatePromotion);
  } catch (err) {
    res.json(err);
  }
};
//Get delete story
module.exports.deletePromotion = async (req, res) => {
  try {
    const promotion = req.params.id;
    const deleteProduct = await Promotion.findByIdAndDelete(promotion);
    return res.json(deleteProduct);
  } catch (err) {
    res.json({ err_message: err });
  }
};
