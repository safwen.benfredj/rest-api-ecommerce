const User = require("../models/user.models");

const getUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    return res.json(err);
  }
};
const getUser = async (req, res) => {
  const id = req.params.id;

  try {
    const user = await User.findById(id);
    return res.json(user.authToJSON());
  } catch (err) {
    return res.json(err);
  }
};
const getAllAdmins = async (req, res) => {
  try {
    const result = await User.find({ isAdmin: true });
    res.json(result);
  } catch (ex) {
    console.log(ex);
  }
};
const updateUser = async (req, res) => {
  const id = req.params.id;
  console.log("My user", id);
  try {
    const dataToUpdate = req.body;
    const { ...updateData } = dataToUpdate;
    const updateUser = await User.findByIdAndUpdate(id, updateData, {
      new: true,
      runValidators: true,
      context: "query",
    });
    return res.json(updateUser);
  } catch (err) {
    return res.json(err);
  }
};

const deleteUser = async (req, res) => {
  const id = req.params.id;

  try {
    const deleteUser = await User.findByIdAndDelete(id);
    return res.json(deleteUser);
  } catch (err) {
    return res.json(err);
  }
};
module.exports.getUser = getUser;
module.exports.getUsers = getUsers;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
module.exports.getAdmins = getAllAdmins;
