const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    fullname: { type: String, required: true, min: 7, max: 128 },
    email: {
      type: String,
      required: [true, "can't be blank"],
      lowercase: true,
      unique: true,
    },
    password: { type: String, required: [true, "can't be blank"], max: 1024 },
    phoneNumber: { type: Number },
    address: { type: String },
    isAdmin: { type: Boolean, default: false },
    cart: { type: mongoose.Schema.Types.ObjectId, ref: "Cart" },
  },
  { timestamps: true }
);

UserSchema.plugin(uniqueValidator, { message: "is already taken." });

UserSchema.methods.profileToJSON = function () {
  return {
    fullname: this.fullname,
    phoneNumber: this.phoneNumber,
    address: this.address,
  };
};

UserSchema.methods.authToJSON = function () {
  return {
    _id: this._id,
    fullname: this.fullname,
    email: this.email,
    isAdmin: this.isAdmin,
    cart: this.cart,
    address: this.address,
    phone:this.phoneNumber
  };
};

module.exports = mongoose.model("User", UserSchema) || mongoose.models.User;
