const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
//const subCategory = require("./subcategory.model");
const Schema = mongoose.Schema;

const CategorySchema = new Schema(
  {
    categoryName: {
      type: String,
      min: 6,
      max: 255,
      index: true,
      required: true,
    },
    subCategories: [
      {
        _id: mongoose.Schema.Types.ObjectId,
        subName: String,
      },
    ],
  },
  { timestamps: true }
);

CategorySchema.plugin(uniqueValidator, { message: "is already taken." });

module.exports =
  mongoose.model("Category", CategorySchema) || mongoose.models.Category;
