const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const DeliverySchema = new Schema(
  {
    price: Number,
  },
  { timestamps: true }
);

module.exports =
  mongoose.model("Delivery", DeliverySchema) || mongoose.models.Delivery;
