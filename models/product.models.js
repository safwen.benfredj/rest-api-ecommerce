const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    productName: {
      type: String,
      min: 6,
      max: 255,
      index: true,
      required: true,
    },
    description: { type: String },
    price: { type: Number, required: true, default: 0 },
    Promotionprice: { type: Number, default: 0 },
    productImage: { type: String, default: "", required: true },
    quantity: { type: Number, required: true, default: 0 },
    Category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
    SubCateg: { type: mongoose.Schema.Types.ObjectId },
    InPromotion: { type: Boolean, default: false },
  },
  { timestamps: true }
);

ProductSchema.plugin(uniqueValidator, { message: "is already taken." });

module.exports =
  mongoose.model("Product", ProductSchema) || mongoose.models.Product;
