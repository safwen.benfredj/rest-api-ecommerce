const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const PomotionSchema = new Schema(
  {
    PoromotionTitle: {
      type: String,
      min: 6,
      max: 255,
      index: true,
      required: true,
    },
    Description: { type: String, min: 6, max: 255, required: true },
    promotionImage: { type: String, default: "", required: true },
  },
  { timestamps: true }
);

PomotionSchema.plugin(uniqueValidator, { message: "is already taken." });

module.exports =
  mongoose.model("Promotion", PomotionSchema) || mongoose.models.Promotion;
