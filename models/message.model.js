const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const messageSchema = new Schema(
  {
    fullname: { type: String, required: true, min: 7, max: 128 },
    email: {
      type: String,
      required: [true, "can't be blank"],
      lowercase: true,
      unique: true,
    },
    message: String,
  },
  { timestamps: true }
);


module.exports = mongoose.model("Message", messageSchema) || mongoose.models.Message;
